from django.shortcuts import render
from .forms import FormJadwal
from .models import JadwalPribadi
from django.shortcuts import redirect

def index(request):
    return render(request, 'index.html')

def tentang(request):
    return render(request, 'tentang.html')

def pengalaman(request):
    return render(request, 'pengalaman.html')

def blog(request):
    return render(request, 'blog.html')

def kontak(request):
    return render(request, 'kontak.html')

def tamu(request):
	return render(request, 'tamu.html')

def jadwalBaru(request):
    if request.method == 'POST':
        form = FormJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwalBaru')

    else:
        form = FormJadwal()
    return render(request, 'jadwal_baru.html', {'form': form})

def daftarJadwal(request):
	dataJadwal = JadwalPribadi.objects.all()
	return render(request, 'daftar_jadwal.html', {'dataJadwal': dataJadwal})

def hapusJadwal(request):
    print("masuk")
    JadwalPribadi.objects.all().delete()
    dataJadwal = JadwalPribadi.objects.all()
    return render(request, 'daftar_jadwal.html', {'dataJadwal': dataJadwal})