from django import forms
from .models import JadwalPribadi

class FormJadwal(forms.ModelForm):
    
    class Meta:
    	model = JadwalPribadi
    	fields = ('hari', 'tanggal', 'jam', 'nama_kegiatan', 'tempat', 'kategori')