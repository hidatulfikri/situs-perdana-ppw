from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
    hari = models.CharField(max_length=255)
    tanggal = models.DateField()
    jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length=255)
    tempat = models.CharField(max_length=255)
    kategori = models.CharField(max_length=255)